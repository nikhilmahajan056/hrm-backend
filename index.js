const express = require("express");
const http = require("http");
const cors = require("cors");
const crypto = require("crypto");
const app = express();
const server = http.createServer(app);
const bodyParser = require('body-parser');
const pinataSDK = require('@pinata/sdk');
require ('dotenv').config();

const pinata = pinataSDK(process.env.PINATA_API_KEY, process.env.PINATA_SECRET_API_KEY);

const corsOptions ={
    "Access-Control-Allow-Origin":'*', 
    "Access-Control-Allow-Methods": 'OPTIONS, GET, POST',
    "Access-Control-Max-Age":200,
    "Access-Control-Allow-Headers": "Content-Type"
 }

app.use(cors(corsOptions));
app.use(bodyParser.json())

const PORT = process.env.PORT || 5000;

app.post('/encrypt-data', function (req, res) {
    // Defining key
    const key = crypto.scryptSync(req.body.randomString, req.body.sign, 32); //hash and signature

    // Defininf iv
    const iv = Buffer.alloc(16, req.body.randomString);
    
    // Creating cipher
    const cipher = crypto.createCipheriv(process.env.ENCRYPTION_ALGORITHM, key, iv);

    let encrypted = cipher.update(JSON.stringify(req.body.userData), 'utf8', 'hex');
    encrypted += cipher.final('hex');

    res.status(200).json({ encryptedData: encrypted })
})

app.post('/decrypt-data', function (req, res) {
    const randomString = req.body.encryptionKey.substring(0, 64);
    const sign = req.body.encryptionKey.substring(64);

    // Defining key
    const key = crypto.scryptSync(randomString, sign, 32); //hash and signature

    // Defining iv
    const iv = Buffer.alloc(16, randomString);

    // Creating decipher
    const decipher = crypto.createDecipheriv(process.env.ENCRYPTION_ALGORITHM, key, iv);

    let encryptedText = Buffer.from(req.body.userData, 'hex');

    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);

    res.status(200).json({ decryptedData: decrypted.toString() })
})

app.post('/upload-to-ipfs', function (req, res) {
    console.log("req.body:", req.body);
    pinata.pinJSONToIPFS(req.body).then((result) => {
        //handle results here
        console.log(result);
        res.status(200).json({ ipfsHash: result })
    }).catch((err) => {
        //handle error here
        console.log(err);
        res.status(500).json({ error: err })
    });
})

server.listen(PORT, () =>
  console.log(`Server is running on port http://localhost:${PORT}`)
);