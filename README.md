# Web3 Patient Record Management Example

The example features how to protect the patients data thorugh encryption.

The example shows how to encrypt and decrypt the data thorugh API calls.

## How to use

Clone the repository and execute below command:

```bash
cd [folder-name]
```

For Backend -

Rename .env.example file to .env and add all the required variables

After adding all the required variables in the .env file execute below commands in the sequential manner:

```bash
npm install

npm start
```

For FrontEnd

cd client
```

```bash
npm install

cd src
```

```bash
npm start